import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc

app = Dash(__name__)

data=pd.read_csv('C:\\Users\\User\\Documents\\perfect\\project101\\Data_file.csv')
colors = {
    'background': '#111111',
    'text': '#7FDBFF',
    'custom_color': '#FF5733'
}
app.layout = html.Div([
    html.H1("TV viewing by country", style={'text-align': 'center',"background_color":'#004400','border':'5px outset #000','color':'#004400'}),
    
    dcc.Dropdown(
        id="slct_year",
        options=[
            {"label": "2015", "value": 2015},
            {"label": "2016", "value": 2016},
            {"label": "2017", "value": 2017},
            {"label": "2018", "value": 2018}
        ],
        multi=False,
        value=2015,
        style={'width': "40%",'display':'inline-block','padding':10}
    ),
    
    html.Div(id='output_container', children=[]),
    
    dcc.Graph(id='my_scr_tv', figure={}),
    
    dcc.Graph(id='my_bar_tv', figure={})
])

@app.callback(
    [Output(component_id='output_container', component_property='children'),
     Output(component_id='my_scr_tv', component_property='figure'),
     Output(component_id='my_bar_tv', component_property='figure')],
    [Input(component_id='slct_year', component_property='value')]
)
def update_graph(option_slctd):
    container = "The year chosen by user was: {}".format(option_slctd)
    
    dff = data.copy()
    dff = dff[['country', str(option_slctd)]]
    fig_scr = go.Figure(data=go.Choropleth(
        locations=dff['country'],
        locationmode='country names',
        z=dff[str(option_slctd)],
        colorscale='brbg',
        autocolorscale=False,
        reversescale=True,
        marker_line_color='darkgray',
        marker_line_width=0.5,
        colorbar_title = 'Duration (min)'
    ))
    
    # Create bar chart
    dff = data[['country', str(option_slctd)]].sort_values(by=str(option_slctd))
    fig_bar = px.bar(data_frame=dff,
    x='country',
    y=str(option_slctd),
    color='country',
    color_discrete_sequence=px.colors.qualitative.Safe ,title='graph bar'
    )
    
    return container, fig_scr, fig_bar

if __name__ == '__main__':
    app.run_server(debug=True)